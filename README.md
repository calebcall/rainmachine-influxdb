Rainmachine Influxdb
======
Build Status:
------
[![pipeline status](https://gitlab.com/calebcall/rainmachine-influxdb/badges/master/pipeline.svg)](https://gitlab.com/calebcall/rainmachine-influxdb/commits/master)

Using this Image:
------
Running the container
A typical invocation of the container might be:

```
$ docker run -e INFLUXDB_HOST=127.0.0.1 \
      -e INFLUXDB_PORT=8086 \
      -e INFLUXDB_DATABSE=rainmachine \
      -e INFLUXDB_USERNAME=mrsprinkles \
      -e INFLUXDB_PASSWORD=supersecret \
      -e RAINMACHINE_HOST=192.168.1.100 \
      -e RAINMACHINE_PASSWORD=mysecretpassword \
      -e SLEEP=30 \
      --name rainmachine-influxdb \
      calebcall/rainmachine-influxdb:latest
```

The following environment variables can be used:
  - `INFLUXDB_HOST`: Set this to the host influxdb is running on (**default:** `localhost`)
  - `INFLUXDB_PORT`: Set this to the port your influxdb runs on (**default:** `8086`)
  - `INFLUXDB_DATABASE`: Set this to the database to write to (**default:** `rainmachine`)
  - `INFLUXDB_USERNAME`: Set this to the user to use to connect to influxdb with (**default:** `<blank>`)
  - `INFLUXDB_PASSWORD`: Set this to the password for the above user (**default:** `<blank>`)
  - `RAINMACHINE_HOST`: Set this to the ip or hostname of your rainmachine controller (**default:** `localhost`)
  - `RAINMACHINE_PASSWORD`: Set this to the password used to login to your controller (**default:** `rainmachine`)
  - `SLEEP`: Set this to the amount of time (in seconds) you want it to wait between polls (**default:** `30`)

All of the above environment variables are *optional*, if not defined then it will use the **default** values.

Todo:
------
  - ~~Add better logging~~
  - Add support for Rainmachine Cloud (my.rainmachine.com)

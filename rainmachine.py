#!/usr/bin/env python3

import http.client
import ssl
import datetime
import json
import time
import os
import sys
import logging

loglevel = os.getenv('LOGLEVEL', 'INFO')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

import asyncio

from aiohttp import ClientSession

from regenmaschine import Client
from influxdb import InfluxDBClient



async def main() -> None:
    """Create the aiohttp session and run the example."""
    async with ClientSession() as websession:
        client = Client(websession)
        rainmachine_host = os.getenv('RAINMACHINE_HOST', 'localhost')
        rainmachine_password = os.getenv('RAINMACHINE_PASSWORD', 'rainmachine')
        sleep_time = int(os.getenv('SLEEP', '30'))

        await client.load_local(
            rainmachine_host, rainmachine_password, port=8080, ssl=True)

        controllers = client.controllers
        for controller in client.controllers.values():
            # Print some client properties:
            logger.info('Name: {0}'.format(controller.name))
            #logger.debug('Host: {0}'.format(controller.host))
            logger.debug('MAC Address: {0}'.format(controller.mac))
            logger.debug('API Version: {0}'.format(controller.api_version))
            logger.debug('Software Version: {0}'.format(controller.software_version))
            logger.debug('Hardware Version: {0}'.format(controller.hardware_version))

            # Get the device name:
            # str
            name = await controller.provisioning.device_name
            # logger.info('name: {0}'.format(type(name)))
            # logger.info(name)

            # Get all diagnostic information:
            # dict
            diagnostics = await controller.diagnostics.current()
            diagnostics_dict = {}
            for k,v in diagnostics.items():
                try:
                    v+=1
                    v = float(v)
                    diagnostics_dict.update({k:v})
                except:
                    str(v)
                    diagnostics_dict.update({k:v})
                diagnostics_dict.update( {"name": name} )
            diagnostics = [{"measurement": "diagnostics", "tags": diagnostics_dict, "fields": diagnostics_dict}]
            logger.info("Pushing Diagnostics")
            try:
                push_data(diagnostics)
            except:
                e = sys.exc_info()[0]
                logger.warning( "<p>Error: %s</p>" % e )

            #json_body = [{'measurement': measurement,'time': t, 'fields': {field: try: x+1 float(x) except: str(x)}} for t,x in ts.items()]

            # Get all weather parsers:
            # list
            # for parser in await controller.parsers.current():
            #     print(parser)

            # Get all programs:
            # list
            #await controller.programs.all()
            for program in await controller.programs.all(include_inactive=True):
                program_details = await controller.programs.get(program['uid'])
                program_details.pop("wateringTimes",None)
                program_details.pop("startTimeParams",None)
                program_details.pop("frequency",None)
                program_details_dict = {}
                for k,v in program_details.items():
                    try:
                        v+=1
                        v = float(v)
                        program_details_dict.update({k:v})
                    except:
                        str(v)
                        program_details_dict.update({k:v})
                program_details = [{"measurement": "program_details", "tags": program_details_dict, "fields": program_details_dict}]
                logger.info("Pushing Program details")
                try:
                    push_data(program_details)
                except:
                    e = sys.exc_info()[0]
                    logger.warning( "<p>Error: %s</p>" % e )

            # Include inactive programs:
            #programs = await controller.programs.all(include_inactive=True)

            # Get a specific program:
            # dict
            #program_1 = await controller.programs.get(1)
            #logger.info(program_1)
            #program_1 = {"measurement": "program_1", "tags": {"name": name}, "fields": program_1}
            # logger.info('program 1: {0}'.format(type(program_1)))

            # Enable or disable a specific program:
            #await controller.programs.enable(1)
            #await controller.programs.disable(1)

            # Get the next run time for all programs:
            # list
            runs = await controller.programs.next()
            if len(runs) > 0:
                for run in runs:
                    run_dict = {}
                    for k,v in run.items():
                        try:
                            v+=1
                            v = float(v)
                            run_dict.update({k:v})
                        except:
                            str(v)
                            run_dict.update({k:v})
                    future_run = [{"measurement": "future_run", "tags": run_dict, "fields": run_dict}]
                    logger.info("Pushing Next Run info")
                    try:
                        push_data(future_run)
                    except:
                        e = sys.exc_info()[0]
                        logger.warning( "<p>Error: %s</p>" % e )

            # Get all running programs:
            # list
            running = await controller.programs.running()
            if len(running) > 0:
                for stuff in running:
                    stuff_dict = {}
                    for k,v in stuff.items():
                        try:
                            v+=1
                            v = float(v)
                            stuff_dict.update({k:v})
                        except:
                            str(v)
                            stuff_dict.update({k:v})
                    programs_running = [{"measurement": "programs_running", "tags": stuff_dict, "fields": stuff_dict}]
                    #logger.info(programs_running)
                    logger.info("Pushing Programs Running")
                    try:
                        push_data(programs_running)
                    except:
                        e = sys.exc_info()[0]
                        logger.warning( "<p>Error: %s</p>" % e )
            # logger.info('programs running: {0}'.format(type(programs_running)))

            # Start and stop a program:
            #await controller.programs.start(1)
            #await controller.programs.stop(1)

            # Get basic details about all zones:
            # list
            # zones = {'zones':await controller.zones.all()}
            zones = await controller.zones.all()
            for zone in zones:
                zone_dict = {}
                for k,v in zone.items():
                    try:
                        v+=1
                        v = float(v)
                        zone_dict.update({k:v})
                    except:
                        str(v)
                        zone_dict.update({k:v})
                zone_info = [{"measurement": "zone", "tags": zone_dict, "fields": zone_dict}]
                logger.info("Pushing zone {0}".format(zone['name']))
                try:
                    push_data(zone_info)
                except:
                    e = sys.exc_info()[0]
                    logger.warning( "<p>Error: %s</p>" % e )
            # logger.info('zones: {0}'.format(type(zones)))

            # Get advanced details about all zones:
            # list
            zones_detail = await controller.zones.all(details=True, include_inactive=True)
            for detail in zones_detail:
                detail.pop("waterSense",None)
                detail_dict = {}
                for k,v in detail.items():
                    try:
                        v+=1
                        v = float(v)
                        detail_dict.update({k:v})
                    except:
                        str(v)
                        detail_dict.update({k:v})

                zone_detail = [{"measurement": "zone_detail", "tags": detail_dict, "fields": detail_dict}]
                logger.info("Pushing zone detail for {0}".format(detail['name']))
                try:
                    push_data(zone_detail)
                except:
                    e = sys.exc_info()[0]
                    logger.warning( "<p>Error: %s</p>" % e )
            # logger.info('zones detail: {0}'.format(type(zones_detail)))

            # Include inactive zones:
            #zones = await controller.zones.all(include_inactive=True)

            # Get basic details about a specific zone:
            # dict
            # zone_1 = await controller.zones.get(1)
            # logger.info('zone 1: {0}'.format(type(zone_1)))

            # Get advanced details about a specific zone:
            # dict
            # zone_1_detail = await controller.zones.get(1, details=True)
            # logger.info('zone 1 detail: {0}'.format(type(zone_1_detail)))

            # Enable or disable a specific zone:
            #await controller.zones.enable(1)
            #await controller.zones.disable(1)

            # Start a zone for 60 seconds:
            #await controller.zones.start(1, 60)

            # ...and stop it:
            #await controller.zones.stop(1)



            # Get all provisioning settings:
            # dict
            settings = await controller.provisioning.settings()
            settings['system'].pop("zoneDuration", None)
            settings_dict = {}
            for k,v in settings.items():
                try:
                    v+=1
                    v = float(v)
                    settings_dict.update({k:v})
                except:
                    str(v)
                    settings_dict.update({k:v})
            settings = [{"measurement": "settings", "tags": settings_dict['system'], "fields": settings_dict['system']}]
            logger.info("Pushing Settings")
            try:
                push_data(settings)
            except:
                e = sys.exc_info()[0]
                logger.warning( "<p>Error: %s</p>" % e )
            # logger.info('settings: {0}'.format(type(settings)))

            # Get all networking info related to the device:
            # dict
            wifi = await controller.provisioning.wifi()
            wifi_dict = {}
            for k,v in wifi.items():
                try:
                    v+=1
                    v = float(v)
                    wifi_dict.update({k:v})
                except:
                    str(v)
                    wifi_dict.update({k:v})
            wifi = [{"measurement": "wifi", "tags": wifi_dict, "fields": wifi_dict}]
            logger.info("Pushing wifi info")
            try:
                push_data(wifi)
            except:
                e = sys.exc_info()[0]
                logger.info( "<p>Error: %s</p>" % e )
            # logger.info('wifi: {0}'.format(type(wifi)))

            # Get various types of active watering restrictions:
            # dict
            current = await controller.restrictions.current()
            current_dict = {}
            for k,v in current.items():
                try:
                    v+=1
                    v = float(v)
                    current_dict.update({k:v})
                except:
                    str(v)
                    current_dict.update({k:v})
            current = [{"measurement": "current_restrictions", "tags": current_dict, "fields": current_dict}]
            logger.info("Pushing Current Restrictions")
            try:
                push_data(current)
            except:
                e = sys.exc_info()[0]
                logger.warning( "<p>Error: %s</p>" % e )
            # logger.info('current: {0}'.format(type(current)))
            # dict
            universal = await controller.restrictions.universal()
            universal_dict = {}
            for k,v in universal.items():
                try:
                    v+=1
                    v = float(v)
                    universal_dict.update({k:v})
                except:
                    str(v)
                    universal_dict.update({k:v})
            universal = [{"measurement": "universal_restrictions", "tags": universal_dict, "fields": universal_dict}]
            logger.info("Pushing Universal Restrictions")
            try:
                push_data(universal)
            except:
                e = sys.exc_info()[0]
                logger.warning( "<p>Error: %s</p>" % e )
            # logger.info('universal: {0}'.format(type(universal)))

            # list
            #hourly = {'hourly':await controller.restrictions.hourly()}
            #logger.info(hourly)
            # logger.info('hourly: {0}'.format(type(hourly)))

            # dict
            #raindelay = await controller.restrictions.raindelay()
            #logger.info(raindelay)
            # logger.info('raindelay: {0}'.format(type(raindelay)))

            # Get watering stats:
            # dict
            today = await controller.stats.on_date(date=datetime.date.today())
            today.pop("vibration",None)
            today.pop("simulatedVibration",None)
            today_dict = {}
            for k,v in today.items():
                try:
                    v+=1
                    v = float(v)
                    today_dict.update({k:v})
                except:
                    str(v)
                    today_dict.update({k:v})
            today = [{"measurement": "today_stats", "tags": today_dict, "fields": today_dict}]
            logger.info("Pushing today's stats")
            try:
                push_data(today)
            except:
                e = sys.exc_info()[0]
                logger.warning( "<p>Error: %s</p>" % e )
            # logger.info('today: {0}'.format(type(today)))

            # list
            upcoming_days = await controller.stats.upcoming(details=False)
            for day in upcoming_days:
                day.pop("vibration",None)
                day.pop("simulatedVibration",None)
                day_dict = {}
                for k,v in day.items():
                    try:
                        v+=1
                        v = float(v)
                        day_dict.update({k:v})
                    except:
                        str(v)
                        day_dict.update({k:v})
                logger.info("Pushing Upcoming day {0} stats".format(day['day']))
                day = [{"measurement": "upcoming_stats", "tags": day_dict, "fields": day_dict}]
                try:
                    push_data(day)
                except:
                    e = sys.exc_info()[0]
                    logger.warning( "<p>Error: %s</p>" % e )
            # logger.info('upcoming days: {0}'.format(type(upcoming_days)))

            # Get info on various watering activities not already covered:
            #log = await controller.watering.log(date=datetime.date.today(), 2)
            # list
            queue = await controller.watering.queue()
            #logger.info(type(queue))
            if len(queue) > 0:
                for togo in queue:
                    togo_dict = {}
                    for k,v in togo.items():
                        try:
                            v+=1
                            v = float(v)
                            togo_dict.update({k:v})
                        except:
                            str(v)
                            togo_dict.update({k:v})
                    logger.info("Pushing Zone {0} Queue info".format(togo['zid']))
                    togo = [{"measurement": "queue_info", "tags": togo_dict, "fields": togo_dict}]
                    push_data(togo)
            # logger.info('queue: {0}'.format(type(queue)))
            # list
            runs = await controller.watering.runs(date=datetime.date.today())
            for run in runs:
                run_dict = {}
                for k,v in run.items():
                    try:
                        v+=1
                        v = float(v)
                        run_dict.update({k:v})
                    except:
                        str(v)
                        run_dict.update({k:v})
                logger.info("Pushing Run Info")
                run = [{"measurement": "runs", "tags": run_dict, "fields": run_dict}]
                push_data(run)
            #logger.info(runs)
            # logger.info('runs: {0}'.format(type(runs)))

            # Pause all watering activities for 30 seconds:
            #await controller.watering.pause_all(30)

            # Unpause all watering activities:
            #await controller.watering.unpause_all()

            # Stop all watering activities:
            #await controller.watering.stop_all()

            logger.info("Sleeping...")
            # time.sleep(sleep_time)


def push_data(data):
    influxdb_host = os.getenv('INFLUXDB_HOST', 'localhost')
    influxdb_port = os.getenv('INFLUXDB_PORT', '8086')
    influxdb_database = os.getenv('INFLUXDB_DATABASE', 'rainmachine')
    influxdb_username = os.getenv('INFLUXDB_USERNAME', '')
    influxdb_password = os.getenv('INFLUXDB_PASSWORD', '')

    client = InfluxDBClient(host=influxdb_host, port=influxdb_port, username=influxdb_username, password=influxdb_password)
    dblist = client.get_list_database()
    list = []
    for db in dblist:
        list.append(db['name'])
    if influxdb_database in list:
        client.write_points(data, database=influxdb_database)
    else:
        client.create_database(influxdb_database)
        client.write_points(data, database=influxdb_database)
        logger.warning("Created Database")

if __name__ == '__main__':
    # while True:
    asyncio.get_event_loop().run_until_complete(main())

FROM alpine:latest

RUN apk add --update python3 python3-dev musl-dev expat gcc &&\
    pip3 install --upgrade pip &&\
    mkdir /rainmachine &&\
    pip3 install regenmaschine influxdb &&\
    adduser -D rainmachine

COPY rainmachine.py /rainmachine/
COPY run.sh /rainmachine/

RUN chmod +x /rainmachine/*

USER rainmachine

CMD ["/rainmachine/run.sh"]
